const { Tee } = require('node-tee');
const Factory = require('./factory');
const { status, error } = require('./constants');

const str = {
  name: 'test name',
  ctrl: 'test',
  path: 'testPath',
};

const myTee = Tee.new('Factory module');
myTee.case('Factory should produce new raw object', expect => {
  const f = Factory.new(str.name);
  expect(f.name).toBe(str.name);
  expect(f.routes).toBe(undefined);
});

module.exports = myTee;
