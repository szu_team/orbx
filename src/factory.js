const { EventEmitter } = require('events');
const { status, error } = require('./constants');

const Factory = {
  new(name, { routes } = {}) {
    const F = Object.create(this);
    F.name = name;
    F.services = {};
    F.status = status.INITIALIZED;

    // {ctrl: <service name>, path: <route path>}
    F.routes = routes;

    return F;
  },
};

module.exports = Factory;
