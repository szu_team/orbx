const test = {
  run1() {
    const r = Register.new();
    const s1 = {
      name: 'test1',
      deps: ['test2'],
    };
    const s2 = {
      name: 'test2',
      deps: [],
    };
    r.registerService(s1);
    r.registerService(s2);
  },
};

module.exports = {
  Register,
  test,
};
