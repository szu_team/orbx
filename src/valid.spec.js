const { Tee } = require('node-tee');
const Valid = require('./valid').validService;

const myTee = Tee.new('Valid module');

myTee.case('Should validate correct service', expect => {
  expect(Valid({ regEmitters: [], name: 'test' })).toBe(true);
  expect(Valid({ name: 'test' })).toBe(false);
  expect(Valid({ regEmitters: [] })).toBe(false);
});

module.exports = myTee;