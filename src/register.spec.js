const { Tee } = require('node-tee');
const Register = require('./register');
const Valid = require('./valid.js');

const myTee = Tee.new('Register module');
myTee.case('Should register service', expect => {
  const obj = Object.assign({}, Register, Valid, {services: {}});
  const testObj = {
    name: 'test',
    regEmitters: [],
  };
  obj.registerService(testObj);
  expect(obj.services['test'].name).toBe('test');
});
// myTee.case(
//   'Factory should fail on pointing to not existing service',
//   expect => {
//     const f = Factory.new(str.name);
//     f.routes = [
//       {
//         ctrl: str.ctrl,
//         path: str.path,
//       },
//     ];
//     try {
//       f.initRoutes({ use() {} });
//     } catch (e) {
//       expect(e.message).toBe(error.NO_REG_CTRL(str.ctrl));
//     }
//   },
// );

module.exports = myTee;