const { error, status } = require('./constants');

module.exports = {
  start(app) {
    this.confirmServices();
    this.initEmitters();
    this.initRoutes(app);
  },
  confirmServices() {
    this.services.forEach(service => {
      if (service.deps.length) {
        const notRegistered = [];
        for (service in service.deps) {
          const lookup = this.services.some(s => s.name === service);
          if (!lookup) {
            notRegistered.push(service);
          }
        }
        if (notRegistered.length) {
          console.log(notRegistered);
          throw Error(error.NO_DEP(service));
        }
      }
    });
  },
};
