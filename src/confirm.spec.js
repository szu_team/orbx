const { Tee } = require('node-tee');
const Confirm = require('./confirm');

const str = {
  service1: {
    name: 'test 1',
    deps: ['test2'],
  },
};

const myTee = Tee.new('Confirm Module');
myTee.cas('Should confirm all services', expect => {
  const initObj = Object.assign({}, Confirm, {});
});
