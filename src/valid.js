module.exports = {
  validService(service) {
    if (!service.regEmitters) {
      return false;
    }
    if (!service.name) {
      return false;
    }
    return true;
  },
};