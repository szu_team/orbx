Register.prototype.createService = ({
  name,
  init,
  initEmittter,
  initRouter,
  initService,
}) => {
  return {
    name,
    model: null,
    emitter: new EventEmitter(),
    router: null,
    depEmitters: [],
    init({ model, service }) {
      this.model = model || null;
    },
    regDepEmitter(fn) {
      this.depEmitters.push(fn);
    },
    initDepEmitters() {
      this.depEmitters.forEach(e => e());
    },
  };
};
