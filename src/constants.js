module.exports = {
  status: {
    READY: 'ready',
    INITED: 'initialized',
  },
  error: {
    NO_DEP: name => `Dependiency not initialized in service: ${name}`,
    NO_NAME: () => `Service doesn't have name`,
    NO_DEP_REG: name => `Service: ${name} doesn't have dependiencies registry`,
    NO_REG_CTRL: name => `${name} service is not registered`,
    SERVICE_EXISTS: name => `${name} service already exists`,
  },
};
