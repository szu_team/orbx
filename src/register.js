const { error } = require('./constants');

module.exports = {
  registerService(service) {
    if (!service.name) {
      throw Error(error.NO_NAME);
    }
    const validation = this.validService(service);
    if (!validation) {
      throw Error(error.NO_DEP_REG(service.name));
    }
    if (this.services[service.name]) {
      throw Error(error.SERVICE_EXISTS(service.name));
    }
    this.services[service.name] = service;
    if (service.route) {
      this.routes.push({
        ctrl: service.name,
        path: service.path,
      });
    }
  },
  initRoutes(app) {
    this.routes.forEach(r => {
      if (!this.services[r.ctrl]) {
        throw Error(error.NO_REG_CTRL(r.ctrl));
      }
      app.use(r.path, this.services[r.ctrl]);
    });
  },
  initEmitters() {
    this.services.forEach(serice => {
      if (service.regEmitters) {
        service.regEmitters.forEach(emitter => {
          if (!this.services[emitter.name]) {
            throw Error(error.NO_DEP(emitter.name));
          }
          emitter.fn(this.services[emitter.name].emitter);
        });
      }
    });
  },
};
