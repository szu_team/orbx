const { Pot } = require('node-tee');

const FactoryTest = require('../src/factory.spec');
const ValidTest = require('../src/valid.spec');
const RegisterTest = require('../src/register.spec');

const myPot = Pot.new('Orbx tester');
myPot.register([FactoryTest, ValidTest, RegisterTest]);

myPot.run();
